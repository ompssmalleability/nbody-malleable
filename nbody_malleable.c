/* nbody.c */
/*
#pragma offload_attribute (push,target(mic))
#include "include/kernels.h"
#pragma offload_attribute (pop)
 */
#include "include/nbody.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <mpi.h>
#undef NDEBUG
#include <assert.h>
#include <sys/mman.h>

#include "dmr.h"
#define DMR_MIN 1
#define DMR_MAX 20
#define DMR_PREF 0

void recvEx(MPI_Datatype mpi_particles_block_t, particles_block_t **local, particles_block_t **tmp, force_block_t **forces, int *n_blocks) {
    int my_rank, comm_size, parent_size, src, factor;
    MPI_Comm_rank(MPI_COMM_WORLD, &my_rank);
    MPI_Comm_size(MPI_COMM_WORLD, &comm_size);
    MPI_Comm_remote_size(DMR_INTERCOMM, &parent_size);
    MPI_Status status;

    
    int sizeParticles, sizeForces;
    factor = comm_size / parent_size;
    (*n_blocks) = (*n_blocks) / factor;
    sizeParticles = (*n_blocks) * sizeof (particles_block_t);
    sizeForces = (*n_blocks) * sizeof (particles_block_t);
    *local = (particles_block_t *) mmap(NULL, sizeParticles, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS, -1, 0);
    *tmp = (particles_block_t *) mmap(NULL, sizeParticles, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS, -1, 0);
    *forces = (force_block_t *) mmap(NULL, sizeForces, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS, -1, 0);
    src = my_rank / factor;
    //dmr_mpi_recv((*local), (*n_blocks), mpi_particles_block_t, src, 0);
    MPI_Recv(*local, (*n_blocks), mpi_particles_block_t, src, 0, DMR_INTERCOMM, &status);
}

void recvSh(MPI_Datatype mpi_particles_block_t, particles_block_t **local, particles_block_t **tmp, force_block_t **forces, int *n_blocks) {
    int my_rank, comm_size, parent_size, src, factor, iniPart, i;
    MPI_Comm_rank(MPI_COMM_WORLD, &my_rank);
    MPI_Comm_size(MPI_COMM_WORLD, &comm_size);
    MPI_Comm_remote_size(DMR_INTERCOMM, &parent_size);
    MPI_Status status;

    int sizeParticles, sizeForces, parent_n_blocks;
    factor = parent_size / comm_size;
    parent_n_blocks = (*n_blocks);
    (*n_blocks) = parent_n_blocks * factor;
    sizeParticles = (*n_blocks) * sizeof (particles_block_t);
    sizeForces = (*n_blocks) * sizeof (particles_block_t);
    *local = (particles_block_t *) mmap(NULL, sizeParticles, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS, -1, 0);
    *tmp = (particles_block_t *) mmap(NULL, sizeParticles, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS, -1, 0);
    *forces = (force_block_t *) mmap(NULL, sizeForces, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS, -1, 0);
    assert(local);
    assert(tmp);
    assert(forces);
    for (i = 0; i < factor; i++) {
        src = my_rank * factor + i;
        iniPart = parent_n_blocks * i;
        //dmr_mpi_recv((*local)+iniPart, parent_n_blocks, mpi_particles_block_t, src, 0);
        MPI_Recv((*local) + iniPart, parent_n_blocks, mpi_particles_block_t, src, 0, DMR_INTERCOMM, &status);
    }
}

void sendEx(MPI_Datatype mpi_particles_block_t, particles_block_t *local, int n_blocks) {
    int my_rank, comm_size, intercomm_size, factor, dst, iniPart, i;
    MPI_Comm_rank(MPI_COMM_WORLD, &my_rank);
    MPI_Comm_size(MPI_COMM_WORLD, &comm_size);
    MPI_Comm_remote_size(DMR_INTERCOMM, &intercomm_size);

    int local_n_blocks;
    factor = intercomm_size / comm_size;
    local_n_blocks = n_blocks / factor;
    MPI_Request *requests = malloc(sizeof (MPI_Request) * factor);
    for (i = 0; i < factor; i++) {
        dst = my_rank * factor + i;
        iniPart = local_n_blocks * i;
        //printf("[%d/%d] (sergio): %s(%s,%d) %d %d\n", my_rank, comm_size, __FILE__, __func__, __LINE__, local_n_blocks, local_n_blocks * sizeof(particles_block_t));
        //dmr_mpi_send(local + iniPart, local_n_blocks, mpi_particles_block_t, dst, 0);
        MPI_Send(local + iniPart, local_n_blocks, mpi_particles_block_t, dst, 0, DMR_INTERCOMM);
    }
}

void sendSh(MPI_Datatype mpi_particles_block_t, particles_block_t *local, int n_blocks) {
    int my_rank, comm_size, intercomm_size, factor, dst;
    MPI_Comm_rank(MPI_COMM_WORLD, &my_rank);
    MPI_Comm_size(MPI_COMM_WORLD, &comm_size);
    MPI_Comm_remote_size(DMR_INTERCOMM, &intercomm_size);

    factor = comm_size / intercomm_size;
    dst = my_rank / factor;
    //printf("[%d/%d] (sergio): %s(%s,%d) %d %d %d\n", my_rank, comm_size, __FILE__, __func__, __LINE__, local_n_blocks, local_n_blocks * sizeof(particles_block_t), dst);
    //dmr_mpi_send(local, n_blocks, mpi_particles_block_t, dst, 0);
    MPI_Send(local, n_blocks, mpi_particles_block_t, dst, 0, DMR_INTERCOMM);
}

void solve_nbody(particles_block_t *local, particles_block_t *tmp, force_block_t *forces,
        int n_blocks, const int timesteps, const float time_interval, int t) {
    int my_rank, comm_size;
    MPI_Comm_rank(MPI_COMM_WORLD, &my_rank);
    MPI_Comm_size(MPI_COMM_WORLD, &comm_size);

    int nitems = 8;
    int blocklengths[] = {BLOCK_SIZE, BLOCK_SIZE, BLOCK_SIZE, BLOCK_SIZE, BLOCK_SIZE, BLOCK_SIZE, BLOCK_SIZE, BLOCK_SIZE};
    int types[] = {MPI_FLOAT, MPI_FLOAT, MPI_FLOAT, MPI_FLOAT, MPI_FLOAT, MPI_FLOAT, MPI_FLOAT, MPI_FLOAT};
    MPI_Datatype mpi_particles_block_t;
    MPI_Aint offsets[nitems];
    offsets[0] = offsetof(particles_block_t, position_x);
    offsets[1] = offsetof(particles_block_t, position_y);
    offsets[2] = offsetof(particles_block_t, position_z);
    offsets[3] = offsetof(particles_block_t, velocity_x);
    offsets[4] = offsetof(particles_block_t, velocity_y);
    offsets[5] = offsetof(particles_block_t, velocity_z);
    offsets[6] = offsetof(particles_block_t, mass);
    offsets[7] = offsetof(particles_block_t, weight);
    MPI_Type_create_struct(nitems, blocklengths, offsets, types, &mpi_particles_block_t);
    MPI_Type_commit(&mpi_particles_block_t);

    DMR_Set_parameters(1,8,0);
    
    for (int i = t; i < timesteps; i++) {
        DMR_RECONFIG(
                solve_nbody(local, tmp, forces, n_blocks, timesteps, time_interval, i),
                sendEx(mpi_particles_block_t, local, n_blocks),
                recvEx(mpi_particles_block_t, &local, &tmp, &forces, &n_blocks),
                sendSh(mpi_particles_block_t, local, n_blocks),
                recvSh(mpi_particles_block_t, &local, &tmp, &forces, &n_blocks));
        // COMPUTE
        if (my_rank == 0)
            printf("(sergio)[%d/%d] %d: %s(%s,%d) PROCESSING STEP %d (blocks %d)\n", my_rank, comm_size, getpid(), __FILE__, __func__, __LINE__, i, n_blocks);
        particles_block_t * remote = local;
        for (int r = 0; r < comm_size; r++) {
            calculate_forces(forces, local, remote, n_blocks);
            exchange_particles(remote, tmp, n_blocks, my_rank, comm_size, r);
            remote = tmp;
        }
        update_particles(n_blocks, local, forces, time_interval);
    }
}
