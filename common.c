#include "include/nbody.h"
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <time.h>
#include <string.h>
#include <mpi.h>
#include <math.h>
#include <ieee754.h>

#undef NDEBUG
#include <assert.h>

#include "mpi.h"
#include <string.h>

void exchange_particles(particles_block_t *sendbuf,
        particles_block_t *recvbuf, int n_blocks, int rank,
        int rank_size, int i) {
    if (i == rank_size - 1)
        return;
    int src, dst;
    //int src = (unsigned int) (rank - 1) % rank_size; //para rank=0 src será igual a rank_size-1
    //int dst = (unsigned int) (rank + 1) % rank_size;

    if (rank == 0)
        src = rank_size - 1;
    else
        src = rank - 1;
    if (rank == rank_size - 1)
        dst = 0;
    else
        dst = rank + 1;
//printf("(sergio): %s(%s,%d)\n", __FILE__, __func__, __LINE__);
    int size = n_blocks * sizeof (particles_block_t);
    //printf("(sergio): %s(%s,%d)\n", __FILE__, __func__, __LINE__);
    if (sendbuf != recvbuf) {
  //      printf("(sergio): %s(%s,%d)\n", __FILE__, __func__, __LINE__);
        MPI_Sendrecv(sendbuf, size, MPI_BYTE,
                dst, 0, recvbuf, size, MPI_BYTE, src,
                0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
    //    printf("(sergio): %s(%s,%d)\n", __FILE__, __func__, __LINE__);
    } else {
      //  printf("(sergio): %s(%s,%d)\n", __FILE__, __func__, __LINE__);
        MPI_Sendrecv_replace(sendbuf, size,
                MPI_BYTE, dst, 0, src, 0,
                MPI_COMM_WORLD, MPI_STATUS_IGNORE);
        //printf("(sergio): %s(%s,%d)\n", __FILE__, __func__, __LINE__);
    }
//    printf("(sergio): %s(%s,%d)\n", __FILE__, __func__, __LINE__);
}

const static float gravitational_constant = 6.6726e-11; /* N(m/kg)2 */

void particle_init(nbody_conf_t * const conf, particles_block_t * const part) {
    for (int i = 0; i < BLOCK_SIZE; i++) {
        //printf("(sergio): %s(%s,%d) - %d - %f - %f\n", __FILE__, __func__, __LINE__, i, conf->domain_size_x * ((float) random() / ((float) RAND_MAX + 1.0)), part->position_x[i]);
        part->position_x[i] =
                conf->domain_size_x * ((float) random() / ((float) RAND_MAX + 1.0));
        part->position_y[i] =
                conf->domain_size_y * ((float) random() / ((float) RAND_MAX + 1.0));
        part->position_z[i] =
                conf->domain_size_z * ((float) random() / ((float) RAND_MAX + 1.0));
        part->mass[i] =
                conf->mass_maximum * ((float) random() / ((float) RAND_MAX + 1.0));
        part->weight[i] = gravitational_constant * part->mass[i];
    }
}

/*
void nbody_generate_particles(nbody_conf_t * conf, nbody_file_t * file) {
    char fname[1024];
    sprintf(fname, "%s.in", file->name);

    if (access(fname, F_OK) == 0)
        return;

    cint fd = open(fname, O_RDWR | O_CREAT | O_TRUNC, S_IRUSR | S_IRGRP | S_IROTH);
    assert(fd >= 0);

    assert(file->total_size % PAGE_SIZE == 0);
    assert(ftruncate(fd, file->total_size) == 0);

    particles_block_t * const particles = mmap(NULL, file->total_size, PROT_WRITE | PROT_READ, MAP_SHARED, fd, 0);

    cint total_num_particles = file->total_size / sizeof (particles_block_t);
    for (int i = 0; i < total_num_particles; i++) {
        particle_init(conf, particles + i);
    }

    assert(munmap(particles, file->total_size) == 0);
    close(fd);
}
 */
void * nbody_generate_particles_local(nbody_conf_t * conf) {
    int rank, rank_size;
    assert(MPI_Comm_size(MPI_COMM_WORLD, &rank_size) == MPI_SUCCESS);
    assert(MPI_Comm_rank(MPI_COMM_WORLD, &rank) == MPI_SUCCESS);

    size_t local_size = conf->num_particles * sizeof (particles_block_t);

    particles_block_t * const particles = mmap(NULL, local_size, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS, -1, 0);
    assert(particles != MAP_FAILED);

    for (int r = -1; r < rank; r++)
    //srand(rank);
    for (int i = 0; i < conf->num_particles; i++)
        particle_init(conf, particles + i);

    return particles;
    //assert(munmap(particles, file->total_size) == 0);
}

/*
void nbody_generate_particles(nbody_conf_t * conf, nbody_file_t * file) {
    char fname[1024];
    sprintf(fname, "%s.in", file->name);

    if (access(fname, F_OK) == 0)
        return;

    cint fd = open(fname, O_RDWR | O_CREAT | O_TRUNC, S_IRUSR | S_IRGRP | S_IROTH);
    assert(fd >= 0);

    assert(file->total_size % PAGE_SIZE == 0);
    assert(ftruncate(fd, file->total_size) == 0);

    particles_block_t * const particles = mmap(NULL, file->total_size, PROT_WRITE | PROT_READ, MAP_SHARED, fd, 0);

    cint total_num_particles = file->total_size / sizeof (particles_block_t);
    for (int i = 0; i < total_num_particles; i++) {
        particle_init(conf, particles + i);
    }

    assert(munmap(particles, file->total_size) == 0);
    close(fd);
}
 */
/*
void * nbody_generate_particles(nbody_conf_t * conf, int rank_size) {

    cint total_num_particles = conf->num_particles*rank_size;
    cint total_size = total_num_particles * sizeof (particles_block_t);

    particles_block_t * const particles = mmap(NULL, total_size, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS, -1, 0);
    assert(particles != MAP_FAILED);

    for (int i = 0; i < total_num_particles; i++) {
        particle_init(conf, particles + i);
    }

    return particles;
    //assert(munmap(particles, file->total_size) == 0);
}
 */
/*
void nbody_generate_particles_global(nbody_conf_t * conf, particles_block_t * particles, int total_num_particles) {
    //char fname[1024];
    //sprintf(fname, "%s.in", file->name);

    //if (access(fname, F_OK) == 0)
    //    return;
    //printf("(sergio): %s\n", fname);

    //cint fd = open(fname, O_RDWR | O_CREAT | O_TRUNC, S_IRUSR | S_IRGRP | S_IROTH);
    //assert(fd >= 0);

    //assert(file->total_size % PAGE_SIZE == 0);
    //assert(ftruncate(fd, file->total_size) == 0);
    //particles_block_t * const particles = mmap(NULL, file->total_size, PROT_WRITE | PROT_READ, MAP_SHARED, fd, 0);

    //cint global_size = total_num_particles * sizeof (particles_block_t);
    //particles = mmap(NULL, global_size, PROT_READ | PROT_WRITE, MAP_SHARED | MAP_ANONYMOUS, -1, 0);

    for (int i = 0; i < total_num_particles; i++) {
        particle_init(conf, particles + i);
    }

    //assert(munmap(particles, file->total_size) == 0);
    //close(fd);
}

void copy_particle(particles_block_t * const orig, particles_block_t * const dest) {
    for (int i = 0; i < BLOCK_SIZE; i++) {
        dest->position_x[i] = orig->position_x[i];
        dest->position_y[i] = orig->position_y[i];
        dest->position_z[i] = orig->position_z[i];
        dest->mass[i] = orig->mass[i];
        dest->weight[i] = orig->weight[i];
    }
}
 */

/*
void * nbody_alloc(const size_t size) {
    //void * const space = mmap(NULL, size, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS, -1, 0);
    void * const space = mmap(NULL, size, PROT_READ | PROT_WRITE, MAP_SHARED | MAP_ANONYMOUS, -1, 0);
    assert(space != MAP_FAILED);
    return space;
}
 */
void * nbody_alloc(const size_t size) {
    void * const space = mmap(NULL, size, PROT_READ | PROT_WRITE,
            MAP_PRIVATE | MAP_ANONYMOUS, -1, 0);
    assert(space != MAP_FAILED);
    return space;
}

force_ptr_t nbody_alloc_forces(nbody_conf_t * const conf) {
    return nbody_alloc(conf->num_particles * sizeof (force_block_t));
}

particle_ptr_t nbody_alloc_particles(nbody_conf_t * const conf) {
    return nbody_alloc(conf->num_particles * sizeof (particles_block_t));
}

/*
particle_ptr_t nbody_alloc_particles_total(nbody_conf_t * const conf, int rank_size) {

    return nbody_alloc(conf->num_particles * rank_size * sizeof (particles_block_t));
}

/*
nbody_file_t nbody_setup_file(nbody_conf_t * const conf) {
    int rank, rank_size;
    assert(MPI_Comm_size(MPI_COMM_WORLD, &rank_size) == MPI_SUCCESS);
    assert(MPI_Comm_rank(MPI_COMM_WORLD, &rank) == MPI_SUCCESS);

    nbody_file_t file;
    cint total_num_particles = conf->num_particles*rank_size;
    file.total_size = total_num_particles * sizeof (particles_block_t);
    file.size = file.total_size / rank_size;
    file.offset = file.size*rank;

    sprintf(file.name, "%s-%s-%d-%d-%d", conf->name,
            XSTR(BIGO), BLOCK_SIZE*total_num_particles, BLOCK_SIZE, conf->timesteps);
    //sprintf(file.name, "sergioResize");

    return file;
}

/*
particles_block_t * nbody_load_particles_local_from_global(nbody_conf_t * conf, particles_block_t * particles, int total_particles, int rank, int rank_size) {

    int group = total_particles / rank_size;
    int offset = group * rank;
    int size = group * sizeof (particles_block_t);

    particles_block_t * local = nbody_alloc(size);

    for (int i = 0; i < group; i++)
        copy_particle(particles + offset + i, local + i);

    return local;
}
 */
nbody_t nbody_setup_empty(nbody_conf_t * const conf) {
    nbody_t nbody = {nbody_alloc_particles(conf),
        nbody_alloc_particles(conf),
        nbody_alloc_forces(conf),
        conf->num_particles,
        conf->timesteps};
    return nbody;
}

nbody_t nbody_setup_local(nbody_conf_t * const conf) {
    nbody_t nbody = {nbody_generate_particles_local(conf),
        nbody_alloc_particles(conf),
        nbody_alloc_forces(conf),
        conf->num_particles,
        conf->timesteps};
    return nbody;
}

/*
nbody_t nbody_setup_global(nbody_conf_t * const conf, int rank, int rank_size) {
    cint total_particles = conf->num_particles * rank_size;
    size_t const global_size = total_particles * sizeof (particles_block_t);
    particles_block_t * const global_particles = mmap(NULL, global_size, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS, -1, 0);
    assert(global_particles != MAP_FAILED);

    //if (rank == 0) // => rank == 0
    nbody_generate_particles_global(conf, global_particles, total_particles);
    assert(MPI_Barrier(MPI_COMM_WORLD) == MPI_SUCCESS);

    nbody_t nbody = {nbody_load_particles_local_from_global(conf, global_particles, total_particles, rank, rank_size),
        nbody_alloc_particles(conf),
        nbody_alloc_forces(conf),
        conf->num_particles,
        conf->timesteps};

    munmap(global_particles, global_size);

    return nbody;
}
 */

particles_block_t * nbody_load_particles(nbody_conf_t * conf, nbody_file_t * file) {

    char fname[1024];
    sprintf(fname, "%s.in", file->name);
    cint fd = open(fname, O_RDONLY, 0);
    assert(fd >= 0);

    void * const ptr = mmap(NULL, file->size, PROT_READ | PROT_WRITE,
            MAP_PRIVATE, fd, file->offset);
    assert(ptr != MAP_FAILED);

    assert(close(fd) == 0);

    return ptr;
}

/*
nbody_t nbody_setup(nbody_conf_t * const conf) {

    nbody_file_t file = nbody_setup_file(conf);

    if (file.offset == 0) // => rank == 0
        nbody_generate_particles(conf, &file);

    assert(MPI_Barrier(MPI_COMM_WORLD) == MPI_SUCCESS);

    nbody_t nbody = {nbody_load_particles(conf, &file),
        nbody_alloc_particles(conf),
        nbody_alloc_forces(conf),
        conf->num_particles,
        conf->timesteps,
        file};
    return nbody;
}
 */

void nbody_save_particles(nbody_conf_t * const conf, nbody_t * const nbody, cint timesteps) {
    int rank, rank_size;
    assert(MPI_Comm_size(MPI_COMM_WORLD, &rank_size) == MPI_SUCCESS);
    assert(MPI_Comm_rank(MPI_COMM_WORLD, &rank) == MPI_SUCCESS);

    nbody_file_t file;
    cint total_num_particles = conf->num_particles*rank_size;
    file.total_size = total_num_particles * sizeof (particles_block_t);
    file.size = file.total_size / rank_size;
    file.offset = file.size*rank;

    sprintf(file.name, "%s-%s-%d-%d-%d", conf->name,
            XSTR(BIGO), BLOCK_SIZE*total_num_particles, BLOCK_SIZE, conf->timesteps);

    cint mode = MPI_MODE_CREATE | MPI_MODE_WRONLY;
    char fname[1024];
    sprintf(fname, "%s.out", file.name);

    MPI_File outfile;
    MPI_File_open(MPI_COMM_WORLD, fname, mode, MPI_INFO_NULL, &outfile);

    size_t offset = file.offset;
    MPI_File_set_view(outfile, offset, MPI_BYTE, MPI_BYTE, "native", MPI_INFO_NULL);

    //printf("(sergio): %s(%s,%d) writing rank %d in %d\n", __FILE__, __func__, __LINE__, rank, offset);
    const size_t size = file.size;
    MPI_File_write(outfile, nbody->local, size, MPI_BYTE, MPI_STATUS_IGNORE);

    MPI_File_close(&outfile);
    assert(MPI_Barrier(MPI_COMM_WORLD) == MPI_SUCCESS);
}

int nbody_check(nbody_conf_t * const conf, const nbody_t *nbody, cint timesteps) {
    int rank, rank_size;
    assert(MPI_Comm_size(MPI_COMM_WORLD, &rank_size) == MPI_SUCCESS);
    assert(MPI_Comm_rank(MPI_COMM_WORLD, &rank) == MPI_SUCCESS);

    nbody_file_t file;
    cint total_num_particles = conf->num_particles*rank_size;
    file.total_size = total_num_particles * sizeof (particles_block_t);
    file.size = file.total_size / rank_size;
    file.offset = file.size*rank;

    //sprintf(file.name, "%s-%s-%d-%d-%d", conf->name,
    //        XSTR(BIGO), BLOCK_SIZE*total_num_particles, BLOCK_SIZE, conf->timesteps);

    char fname[1024];
    sprintf(fname, "file.ref");
    if (access(fname, F_OK) != 0)
        return -1;

    cint fd = open(fname, O_RDONLY, 0);
    assert(fd >= 0);

    particle_ptr_t particles = mmap(NULL, file.size,
            PROT_READ, MAP_SHARED, fd, file.offset);

    double error = 0.0;
    int count = 0;
    for (int i = 0; i < nbody->num_particles; i++) {

        for (int e = 0; e < BLOCK_SIZE; e++) {

            if ((nbody->local[i].position_x[e] != particles[i].position_x[e]) ||
                    (nbody->local[i].position_y[e] != particles[i].position_y[e]) ||
                    (nbody->local[i].position_z[e] != particles[i].position_z[e])) {
                error += fabs(((nbody->local[i].position_x[e] - particles[i].position_x[e])*100.0) / particles[i].position_x[e]) +
                        fabs(((nbody->local[i].position_y[e] - particles[i].position_y[e])*100.0) / particles[i].position_y[e]) +
                        fabs(((nbody->local[i].position_z[e] - particles[i].position_z[e])*100.0) / particles[i].position_z[e]);
                count++;
            }
        }
    }
    double relative_error = error / (3.0 * count);
    if ((count * 100.0) / (nbody->num_particles * BLOCK_SIZE) > 0.6 || (relative_error > 0.000008))
        printf("Relative error[%d]: %f\n", count, relative_error);
    else if (rank == 0)
        printf("Result validation: OK\n");

    return 0;
}

/*
void nbody_save_particles(nbody_t * const nbody, cint timesteps) {
    int myrank, commsize;
    MPI_Comm_rank(MPI_COMM_WORLD, &myrank);
    MPI_Comm_size(MPI_COMM_WORLD, &commsize);

    printf("\tWriting in file %s.out\n", nbody->file.name);

    cint mode = MPI_MODE_CREATE | MPI_MODE_WRONLY;
    char fname[1024];
    sprintf(fname, "%s.out", nbody->file.name);

    MPI_File outfile;
    MPI_File_open(MPI_COMM_WORLD, fname, mode, MPI_INFO_NULL, &outfile);

    const size_t offset = nbody->file.offset;
    MPI_File_set_view(outfile, offset, MPI_BYTE, MPI_BYTE, "native", MPI_INFO_NULL);

    const size_t size = nbody->file.size;
    MPI_File_write(outfile, nbody->local, size, MPI_BYTE, MPI_STATUS_IGNORE);

    MPI_File_close(&outfile);
    assert(MPI_Barrier(MPI_COMM_WORLD) == MPI_SUCCESS);
}
 */
void nbody_free(nbody_t * const nbody) {
    {
        const size_t size = nbody->num_particles * sizeof (particles_block_t);
        assert(munmap(nbody->local, size) == 0);
    }
    //{
    //    const size_t size = nbody->num_particles * sizeof (particles_block_t);
    //    assert(munmap(nbody->remote, size) == 0);
    //}
    {
        const size_t size = nbody->num_particles * sizeof (force_block_t);
        assert(munmap(nbody->forces, size) == 0);
    }
}

double wall_time() {
    struct timespec ts;
    clock_gettime(CLOCK_MONOTONIC, &ts);
    return (double) (ts.tv_sec) + (double) ts.tv_nsec * 1.0e-9;
}
