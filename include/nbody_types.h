#ifndef nbody_types_h
#define nbody_types_h
#ifdef OpenCL_V
#define BLOCK_SIZE 1 //Opencl kernel only works with BLOCK_SIZE==1, as using other number is slower
#else
#define BLOCK_SIZE 128
#endif

#ifndef __OPENCL_VERSION__
#include <stddef.h> //Header not avaiable in OCL
#endif

typedef const int cint;

typedef float * __restrict__ const float_ptr_t   __attribute__((align_value (64)));
typedef struct {
   float position_x[BLOCK_SIZE]; /* m   */
   float position_y[BLOCK_SIZE]; /* m   */
   float position_z[BLOCK_SIZE]; /* m   */
   float velocity_x[BLOCK_SIZE]; /* m/s */
   float velocity_y[BLOCK_SIZE]; /* m/s */
   float velocity_z[BLOCK_SIZE]; /* m/s */
   float mass[BLOCK_SIZE];       /* kg  */
   float weight[BLOCK_SIZE];
} particles_block_t;

typedef particles_block_t * __restrict__ const particle_ptr_t   __attribute__((align_value (64)));
typedef struct {
   float x[BLOCK_SIZE]; /* x   */
   float y[BLOCK_SIZE]; /* y   */
   float z[BLOCK_SIZE]; /* z   */
} force_block_t;

typedef struct {
   float x; /* x   */
   float y; /* y   */
   float z; /* z   */
} single_force;


typedef force_block_t * __restrict__ const force_ptr_t   __attribute__((align_value (64)));

#ifndef _OMPSS
#pragma warning disable 161
#pragma offload_attribute (push,target(mic))
#endif
static inline int log2i(int i){
	return 31-__builtin_clz(i);
}
#ifndef _OMPSS
#pragma offload_attribute (pop)
#pragma warning pop
#endif

#endif /* #ifndef nbody_types_h */
