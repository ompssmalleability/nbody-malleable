#ifndef nbody_h
#define nbody_h

#ifndef BIGO
#define BIGO N
#endif

#include <stddef.h>
#include "nbody_types.h"

#define PART 1024
#define PAGE_SIZE 4096
#define MIN_PARTICLES (4096*BLOCK_SIZE/sizeof(particles_block_t))
#define roundup(x, y) (                                 \
{                                                       \
        const typeof(y) __y = y;                        \
        (((x) + (__y - 1)) / __y) * __y;                \
}                                                       \
)

#define STR(s) #s
#define XSTR(s) STR(s)
#define XCALCULATE_FORCES(s) CALCULATE_FORCES(s)
#define CALCULATE_FORCES(s) calculate_forces_##s
#define calculate_forces XCALCULATE_FORCES(BIGO)

#define TRANSFERPATTERN 1



static const float default_domain_size_x = 1.0e+10; /* m  */
static const float default_domain_size_y = 1.0e+10; /* m  */
static const float default_domain_size_z = 1.0e+10; /* m  */
static const float default_mass_maximum = 1.0e+28; /* kg */
static const float default_time_interval = 1.0e+0; /* s  */
static const int default_seed = 12345;
static const char* default_name = "data/nbody";
static const int default_num_particles = 16384;
static const int default_timesteps = 10;
static const int default_resizeStep = 1; //check resize every step
static const int default_spawnProcesses = 4;

typedef struct {
    size_t total_size;
    size_t size;
    size_t offset;
    char name[1000];
} nbody_file_t;

typedef const struct {
    float domain_size_x;
    float domain_size_y;
    float domain_size_z;
    float mass_maximum;
    float time_interval;
    int seed;
    const char* name;
    const int timesteps;
    const int num_particles;
} nbody_conf_t;

typedef const struct {
    particle_ptr_t const local;
    particle_ptr_t const remote;
    force_ptr_t const forces;
    const int num_particles;
    const int timesteps;
} nbody_t;

/* coomon.c */

//nbody_t nbody_setup(nbody_conf_t * const conf);
//nbody_t nbody_setup_2(nbody_conf_t * const conf);
nbody_t nbody_setup_empty(nbody_conf_t * const conf);
nbody_t nbody_setup_local(nbody_conf_t * const conf);
//nbody_t nbody_setup_global(nbody_conf_t * const conf, int rank, int rank_size);
//void nbody_generate_particles(nbody_conf_t * const conf, nbody_file_t * file);
void * nbody_generate_particles_local(nbody_conf_t * const conf);
//nbody_t nbody_setup_resize(nbody_conf_t * const conf);
void nbody_save_particles(nbody_conf_t * const conf, nbody_t *nbody, cint timesteps);
void nbody_free(nbody_t *nbody);
int nbody_check(nbody_conf_t * const conf, nbody_t *nbody, cint timesteps);

double wall_time();

void exchange_particles(particles_block_t * const sendbuf, particles_block_t * recvbuf, const int n_blocks,
        const int rank, const int rank_size, const int i);
void solve_nbody(particles_block_t * particles, particles_block_t * const tmp, force_block_t * const forces,
        const int num_particles, const int timesteps, const float time_interval, int timestep);

#ifndef _OMPSS
#pragma warning disable 161
#pragma warning push
#pragma offload_attribute (push, target (mic))
#endif
inline void update_particle(const particles_block_t* __restrict__ part, const force_block_t* __restrict__ force, const float time_interval);
inline void update_particles(const int n_blocks, const particles_block_t* __restrict__ block, const force_block_t* __restrict__ forces, const float time_interval);
inline void calculate_forces(const force_block_t* __restrict__ forces, const particles_block_t* __restrict__ block1, const particles_block_t* __restrict__ block2, const int bs);
#ifndef _OMPSS
#pragma warning pop
#pragma offload_attribute (pop)
#endif


#endif /* #ifndef nbody_h */
