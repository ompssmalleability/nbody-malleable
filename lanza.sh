#!/bin/bash

export LD_LIBRARY_PATH=/home/siserte/slurm/lib:/state/partition1/soft/gnu/mpich-3.2/lib:$LD_LIBRARY_PATH
export NX_ARGS="--enable-block --force-tie-master --smp-workers=1"
#export NX_ARGS="--smp-workers=1"

NODELIST="$(scontrol show hostname $SLURM_JOB_NODELIST | paste -d, -s)"
#mpiexec -iface eth0 -n $SLURM_JOB_NUM_NODES -hosts $NODELIST ./nbodymalleable.INTEL64 460787712 100
#mpiexec -iface eth0 -n $SLURM_JOB_NUM_NODES -hosts $NODELIST ./nbodymalleable.INTEL64 3276800 100
mpiexec -iface eth0 -n $SLURM_JOB_NUM_NODES -hosts $NODELIST ./nbodymalleable.INTEL64 409600 100

#multiprocs
#NODELIST="$(python parseCpusPerNode.py)"
#mpiexec -iface eth0 -n $SLURM_NPROCS -hosts $NODELIST ./nbodymalleable.INTEL64 460787712 2
#mpiexec -iface eth0 -n $SLURM_NPROCS -hosts $NODELIST ./nbodymalleable.INTEL64 460780 2
