#N2|NlogN|N
BIGO?=N2

#Flags
#-fast => -xHOST -O3 -ipo -no-prec-div -static
#CFLAGS   = -wd1173 -std=gnu99 -O3 -fp-model fast=2
CFLAGS = -std=gnu99 -O3 -fp-model fast=2 -vec-report

#set compiler prefetching distance, they can be more fine-tuned by setting pragmas in the code, but in this case performance is similar. 
CFLAGS += -mP2OPT_hlo_use_const_pref_dist=16 -mP2OPT_hlo_use_const_second_pref_dist=16

MPIFLAGS    = -I/state/partition1/soft/gnu/mpich-3.2/include -L/state/partition1/soft/gnu/mpich-3.2/lib -lmpi
SLURMFLAGS  = -I/home/siserte/slurm/include -L/home/siserte/slurm/lib -lslurm
DMRFLAGS    = -I/home/siserte/dmr -L/home/siserte/dmr -ldmr
OMPSSFLAGS  = --ompss
FLAGS  = -O3 -k -std=gnu99 

all: nbody_malleable
	
######### O (N^2) computation	##########

nbody_malleable: nbody_malleable.c  kernels/kernel_smp.c common.c main.c
	mpimcc $(FLAGS) $(OMPSSFLAGS) $(MPIFLAGS) $(SLURMFLAGS) $(DMRFLAGS) nbody_malleable.c kernels/kernel_smp.c common.c main.c -o nbodymalleable.INTEL64 -DBIGO=NlogN

clean:
	rm -f *.out *.o kernels/*.o data/*.out *.dot *.pdf *.intel64 *.mic
