/* nbody.c */

#include "include/nbody.h"
#include <stdio.h>
#include <stdlib.h>
#include <mpi.h>
#undef NDEBUG
#include <assert.h>
#include <string.h>
#include <unistd.h>

#include <sys/time.h>

//#include <sys/mman.h>

#define DATAPATH "/home/siserte/SharedWithWilly/nbody-malleable/src/data/malleable"

/***************************
 * 
 * argv[1] particles
 * argv[2] steps
 * argv[3] check resize period (check every n steps) 
 * argv[4] steps made. Initially 0, after spawning changes value
 ***************************/
//mpiexec -n 8 /home/siserte/SharedWithWilly/nbody-malleable/src/nbody.ompss 230393 8 2 0

int main(int argc, char** argv) {
    srand(1);

    MPI_Comm resizeComm;
    int provided;
    char fileName[256];
    struct timeval tv;

    assert(argv[6]);

    MPI_Init_thread(&argc, &argv, MPI_THREAD_MULTIPLE, &provided);
    //   assert(MPI_THREAD_MULTIPLE == provided);

    int rank, rank_size;
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &rank_size);

    int name_len;
    char processor_name[MPI_MAX_PROCESSOR_NAME];
    MPI_Get_processor_name(processor_name, &name_len);

    int num_particles = (argc <= 1 ? default_num_particles : roundup(atoi(argv[1]), MIN_PARTICLES)) / rank_size;
    assert(num_particles >= 4096);
    num_particles = num_particles / BLOCK_SIZE;
    const int input_timesteps = argc <= 2 ? default_timesteps : atoi(argv[2]);
    assert(input_timesteps > 0);
    const int resizeStep = argc <= 3 ? default_resizeStep : atoi(argv[3]); //steps before spawning (default 4)
    int stepsMade = argc <= 4 ? 0 : atoi(argv[4]);
    //sprintf(fileName, "%s", DATAPATH);
    sprintf(fileName, "%s-Steps%d-Break%d", DATAPATH, input_timesteps, resizeStep);

    nbody_conf_t conf = {default_domain_size_x, default_domain_size_y, default_domain_size_z,
        default_mass_maximum, default_time_interval, default_seed, fileName,
        input_timesteps, num_particles};

    int total_num_particles = conf.num_particles * rank_size;

    if (rank == 0) {
        printf("O(%s), mpi ranks: %d, particles: %d, total blocks: %d, local blocks: %d "
                "Check resize every %d steps, Total timesteps: %d (pid: %d)\n",
                XSTR(BIGO), rank_size, num_particles * BLOCK_SIZE * rank_size, num_particles * rank_size, num_particles, resizeStep, input_timesteps, getpid());
        printf("[%d/%d] (sergio): %s(%s,%d) %d %d\n", rank, rank_size, __FILE__, __func__, __LINE__, num_particles, num_particles * sizeof (particles_block_t));
    }
    //printf("(sergio: %d)[%d/%d]: %s(%s,%d)\n", getpid(), rank, rank_size, __FILE__, __func__, __LINE__);
    /**************************************************************************/
    nbody_t nbody = nbody_setup_local(&conf);
    assert(MPI_Barrier(MPI_COMM_WORLD) == MPI_SUCCESS);
    solve_nbody(nbody.local, nbody.remote, nbody.forces, num_particles, input_timesteps, conf.time_interval, 0);
    //solve_nbody_aux(input_timesteps, 0);

    printf("\t(sergio)[%d/%d]: %s(%s,%d)\n", rank, rank_size, __FILE__, __func__, __LINE__);
    //nbody_save_particles(&conf, &nbody, input_timesteps);
    //printf("\t(sergio)[%d/%d]: %s(%s,%d)\n", rank, rank_size, __FILE__, __func__, __LINE__);
    //nbody_free(&nbody);
    MPI_Finalize();
    return 0;
}
