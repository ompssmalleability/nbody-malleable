#include "../include/nbody_types.h"
#include <math.h>
#include <stdbool.h>

#ifndef _OMPSS
#pragma warning disable 161
#pragma warning push
#pragma offload_attribute (push,target(mic))
#endif
const static float gravitational_constant = 6.6726e-11; /* N(m/kg)2 */

static inline void calculate_forces_part(float pos_x1, float pos_y1, float pos_z1, float mass1,
        float pos_x2, float pos_y2, float pos_z2, float mass2,
        float_ptr_t fx, float_ptr_t fy, float_ptr_t fz, const bool safe) {

    const float diff_x = pos_x2 - pos_x1;
    const float diff_y = pos_y2 - pos_y1;
    const float diff_z = pos_z2 - pos_z1;

    const float distance_squared = diff_x * diff_x + diff_y * diff_y + diff_z * diff_z;

    const float distance = sqrtf(distance_squared); // (Optimization) Intel compiler detects and emits rsqrt for this sqrt

    float force = ((mass1 / (distance_squared * distance))*(mass2 * gravitational_constant));

    //This "if" MUST be after calculating force
    if (safe && distance_squared == 0.0f) force = 0.0f;
    *fx += force*diff_x;
    *fy += force*diff_y;
    *fz += force*diff_z;
}

static inline void calculate_forces_BLOCK_(force_ptr_t forces,
        particle_ptr_t block1,
        particle_ptr_t block2,
        const bool safe) {
    float_ptr_t x = forces->x;
    float_ptr_t y = forces->y;
    float_ptr_t z = forces->z;

    float_ptr_t pos_x1 = block1->position_x;
    float_ptr_t pos_y1 = block1->position_y;
    float_ptr_t pos_z1 = block1->position_z;
    float_ptr_t mass1 = block1->mass;

    float_ptr_t pos_x2 = block2->position_x;
    float_ptr_t pos_y2 = block2->position_y;
    float_ptr_t pos_z2 = block2->position_z;
    float_ptr_t mass2 = block2->mass;

#pragma simd vectorlength(32)
    for (int i = 0; i < BLOCK_SIZE; i++) {
        float fx = x[i], fy = y[i], fz = z[i];
#pragma simd reduction(+:fx,fy,fz)
        for (int j = 0; j < BLOCK_SIZE; j++) {
            calculate_forces_part(pos_x1[i], pos_y1[i], pos_z1[i], mass1[i], pos_x2[j], pos_y2[j], pos_z2[j], mass2[j], &fx, &fy, &fz, safe);
        }
        x[i] = fx;
        y[i] = fy;
        z[i] = fz;
    }
}

static inline void calculate_forces_BLOCK(force_ptr_t forces,
        particle_ptr_t block1,
        particle_ptr_t block2) {
    //(Optimization) If we are on not in the same block,
    //do not use safe mode which checks if p1 is the same particle than p2 and corrects NaN forces
    //This "if" with allows intel compiler to remove branch "if (safe && distance_squared...)"
    //at compile-time in calculate_forces_part whenever needed (Do not change it to a single call)
    if (block1 == block2) calculate_forces_BLOCK_(forces, block1, block2, true /*safe*/);
    else calculate_forces_BLOCK_(forces, block1, block2, false /*non-safe*/);
}

void calculate_forces_N2(force_block_t* __restrict__ forces, particles_block_t* __restrict__ block1, particles_block_t* __restrict__ block2, int bs) {
#pragma omp parallel for
    for (int i = 0; i < bs; i++) {
        for (int j = 0; j < bs; j++) {
            calculate_forces_BLOCK(forces + i, block1 + i, block2 + j);
        }
    }
}

void calculate_forces_NlogN(force_block_t* __restrict__ forces, particles_block_t* __restrict__ block1, particles_block_t* __restrict__ block2, int bs) {
#pragma omp parallel for
    for (int i = 0; i < bs; i++) {
        for (int j = 0; j < log2i(bs); j++) {
            calculate_forces_BLOCK(forces + i, block1 + i, block2 + j);
        }
    }
}

void calculate_forces_N(force_block_t* __restrict__ forces, particles_block_t* __restrict__ block1, particles_block_t* __restrict__ block2, int bs) {
#pragma omp parallel for //In OmpSs parallel is ignored
    for (int i = 0; i < bs - 1; i++) {
        calculate_forces_BLOCK(forces + i, block1 + i, block2 + i + 1);
    }
}

void update_particle(particle_ptr_t part, const force_ptr_t force, const float time_interval) {

    for (int e = 0; e < BLOCK_SIZE; e++) {
        const float mass = part->mass[e];
        const float velocity_x = part->velocity_x[e];
        const float velocity_y = part->velocity_y[e];
        const float velocity_z = part->velocity_z[e];
        const float position_x = part->position_x[e];
        const float position_y = part->position_y[e];
        const float position_z = part->position_z[e];

        const float time_by_mass = time_interval / mass;
        const float half_time_interval = 0.5f * time_interval;

        const float velocity_change_x = force->x[e] * time_by_mass;
        const float velocity_change_y = force->y[e] * time_by_mass;
        const float velocity_change_z = force->z[e] * time_by_mass;
        const float position_change_x = velocity_x + velocity_change_x * half_time_interval;
        const float position_change_y = velocity_y + velocity_change_y * half_time_interval;
        const float position_change_z = velocity_z + velocity_change_z * half_time_interval;

        part->velocity_x[e] = velocity_x + velocity_change_x;
        part->velocity_y[e] = velocity_y + velocity_change_y;
        part->velocity_z[e] = velocity_z + velocity_change_z;
        part->position_x[e] = position_x + position_change_x;
        part->position_y[e] = position_y + position_change_y;
        part->position_z[e] = position_z + position_change_z;
    }
}


#include <string.h>

void update_particles(const int n_blocks, particles_block_t* __restrict__ block,
        force_block_t* __restrict__ forces,
        const float time_interval) {
#pragma omp parallel for
    for (int i = 0; i < n_blocks; i++) {
        update_particle(block + i, forces + i, time_interval);
    }
    memset(forces, 0, sizeof (force_block_t) * n_blocks);
}
#ifndef _OMPSS
#pragma offload_attribute (pop)
#pragma warning pop
#endif

//    __assume_aligned(block, 64);
//   __assume_aligned(forces, 64);
//    #pragma vector aligned
